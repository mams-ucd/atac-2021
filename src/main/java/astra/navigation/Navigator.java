package astra.navigation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.apache.jena.sparql.function.library.leviathan.log;

import astra.core.Module;
import astra.core.Module.ACTION;
import astra.core.Module.TERM;
import astra.formula.Formula;
import astra.formula.Predicate;
import astra.messaging.Utilities.PredicateState;
import astra.term.ListTerm;

/*
 * TODO: pass through labels, e.g. <http://www.w3.org/2000/01/rdf-schema#label> "Dressing Room" .
 */ 
public class Navigator extends Module {

    String goalRoom;
    int counter = 0;
    //Just stores url
    Path path = new Path();
    
    Location previous = null;
    Location initial = null;
    LocationStore locationStore = new LocationStore();
    double epsilon = 0.1;
    List<String> labels = new ArrayList<String>();

    @ACTION
    public boolean setGoalRoom(String goal) {
        this.goalRoom = goal;
        return true;
    }

    @ACTION
    public boolean reset() {
        counter = 0;
        previous = null;
        return true;
    }

    /*
     * FOR KNOWLEDGE TRANSFER
     * Don't need to keep scores once have path.
     * Pros: object reuse
     * Cons: object reuse
     * Potentially messy flushing scores
     */
    @TERM
    public LocationStore getAllLocations() {
        return locationStore;
    }

    @TERM
    public Path getPath() {
        //from current, get to goalRoom
        Location current = initial;
        path = new Path();
        path.add("",current.getLabel());
        boolean goalReached = false;
        int steps = 0;
        while (!goalReached) {
            //Exploit policy to get the next room from the current one
            String nextRoomUrl = chooseNextLocation(current);
            //Which direction is it in
            String direction = current.getDirection(nextRoomUrl);
            
            //Update the current room
            current = locationStore.get(nextRoomUrl);
            String roomName = current.getLabel();
            path.add(direction,roomName);
            if (roomName == null || roomName.isEmpty()) {
                System.out.println(path.toString());
                System.out.println("Chosen " + nextRoomUrl);
                System.out.println("Direction " + direction);
                System.out.println("Don't have a room name ");

                HashMap<String, Location> locs = locationStore.getAllLocations();
                for (String key: locs.keySet()) {
                    System.out.println("KEY: " + key);
                    Location l = locs.get(key);
                    System.out.println(l);
                    System.out.println(l.getId());
                    System.out.println(l.getLabel());
                }
            } else if (roomName.equals(goalRoom)) {
                goalReached = true;
            }
            
            steps++;
            if (steps > 1000) {
                System.out.println("Reached limit: too many steps trying to find the way to " + goalRoom);
                return path;
            }
        }
        return path;
    }

    @ACTION
    public boolean acquireAllLocations(LocationStore incomingLocationStore) {
        System.out.println("Adding " + incomingLocationStore.size() + " to my knowledge base.");
        locationStore.add(incomingLocationStore);
        return true;
    }

    @TERM
    public boolean pathContains(String roomName) {
        if (path.contains(roomName)) {
            return true;
        }
        return false;
    }

    @TERM
    public Path subPath(String roomName) {
        return path.subPath(roomName);
    }
    
    @TERM
    public String process(String currentUrl, String labelCurrent, String nextStateBeliefPredicate) {
       
        counter++;
        //Check to ensure the agent isn't infinitely travelling the maze
        if (counter == 10000) {
            System.out.println("Reached my limit.");
            return "error";
        }
        
        //Get the current location information, if I have visited it before.
        Location current = locationStore.get(currentUrl);
        //If not, create a new one, and search beliefs to about the next locations to 
        // build up information on the current state
        if (current == null) {
            current = new Location(currentUrl);
            locationStore.put(currentUrl, current);
        }
        if(current.getAdjacentLocations().isEmpty()) {
            for (Formula belief : agent.beliefs().beliefs()) {
                if (((Predicate) belief).predicate().equals(nextStateBeliefPredicate)) {
                    Predicate b = (Predicate) belief;
                    String direction = b.getTerm(0).toString().replace("\"", ""); 
                    String url = b.getTerm(1).toString().replace("\"", ""); 
                    current.addAdjacentRoom(direction, url);
                    Location loc = locationStore.get(url);
                    if (loc == null) {
                        loc = new Location(url);
                        locationStore.put(url, loc);   
                    }
                }
            }
        }
        //We only get the label when we visit the room, so we may have information about the location 
        // (e.g. via adjacent rooms) but not the label
        current.setLabel(labelCurrent);
        
        //If there isn't a first location already, then this is it
        if (initial == null) {
            initial = current;
        }
        
        if (previous != null) {
            String previousLocation = previous.getId();  
            if (!previousLocation.isEmpty()) {
                if (labelCurrent.equals(goalRoom)) {
                    System.out.println("*GOAL REACHED!*");
                    assignScore(current, previousLocation, 100.00);
                    previous = current;
                    return "";
                }
                assignScore(current, previousLocation, 0.0);
            }          
        }

        previous = current;
        return exploreOrExploit(current);
    }

    public String exploreOrExploit(Location loc) {
        double randomN = Math.random();
        if (randomN <= epsilon) {
            //Explore
            return chooseNextLocationRandom(loc);
        } else {
            return chooseNextLocation(loc);
        }
    }

    //FIXME: not sure if highest location is going to work
    public String chooseNextLocation(Location current) {
        //Do any children have high score?
        double highestScore = 0;
        Location highestScoringLocation = null;
        HashMap<String, String> adjacentLocations = current.getAdjacentLocations();
        for (String url: adjacentLocations.values()) {
            Location loc = locationStore.get(url);
            double score = loc.getScore(current.getId());
            if (score > highestScore) {
                highestScore = score;
                highestScoringLocation = loc;
            }
        } 
        if (highestScoringLocation != null) {
            return highestScoringLocation.getId();
        }
        else {
            return chooseNextLocationRandom(current);
        }
    }

    public String chooseNextLocationRandom(Location current) {
        HashMap<String, String> children = current.getAdjacentLocations();
        Set<String> directions = children.keySet();
        int size = directions.size();
        int randomNumber = (int)(Math.random() * size);

        String randomDirection = (String)directions.toArray()[randomNumber];
        //System.out.println("Randomly going " + randomDirection);
        return children.get(randomDirection);
    }

    /*
     * Where Q(S,A) <- Q(S,A) + alpha[R + (gamma * max Q(S',a) - Q(S,A)] happens
     * Q(S,A) is stored on the location. The action is moving into that location.
     * The state is the previous location. It is the score for the move (A) from S, previous, to current.
     * 
     * max Q(S',a) is the maximum score for moving from the current location, to another one.
     */ 
    public void assignScore(Location current, String previousLocation, double score) {
        //Action is, have moved into current from previous
        //Previous State is Location previous
        //Current State is Location current
        //Infer reward from whether current state == goal state
        double existingValue = current.getScore(previousLocation); //-1 if no existing score
        double highestScoreForNextAction = highestScoreForNextLocations(current);
        double alpha = 0.9; //"learning rate"
        double gamma = 0.9; //can be thought of as degredation 

        double final_score = existingValue + (alpha * (score + (gamma * highestScoreForNextAction) - existingValue));
        current.addScore(previousLocation, final_score);
        
    }

    private double highestScoreForNextLocations(Location current) {
        double highestScore = 0;
        for (String nextLocation : current.adjacentLocations.values()) {
            Location next = locationStore.get(nextLocation);
            if (next != null) {
                double scoreForMovingIntoNext = next.getScore(current.getId()); 
                if (scoreForMovingIntoNext > highestScore) {
                    highestScore = scoreForMovingIntoNext;
                }
            }
        }
        return highestScore;
    }
    
}
