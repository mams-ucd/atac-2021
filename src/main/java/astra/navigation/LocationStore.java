package astra.navigation;

import java.util.HashMap;

public class LocationStore {
    
    HashMap<String, Location> allLocations = new HashMap<>();

    public void add(String roomId, Location loc) {
        allLocations.put(roomId, loc);
    }

    public int size() {
        return allLocations.size();
    }

    public void add(LocationStore other) {
        HashMap<String, Location> otherAllLocs = other.getAllLocations();
        for (Location value: otherAllLocs.values()) {
            value.flushScores();
        }
        allLocations.putAll(otherAllLocs);
    }

    public HashMap<String, Location> getAllLocations() {
        return allLocations;
    }

    public Location get(String object) {
        return allLocations.get(object);
    }

    public void put(String object, Location loc) {
        allLocations.put(object, loc);
    }
}
