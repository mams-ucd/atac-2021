package astra.navigation;

import java.util.ArrayList;
import java.util.List;

//FIXME: store as pairs instead .... 
// need to keep concept of direction. Tuples.
public class Path {
    
    List<Pair> path = new ArrayList<Pair>();

    public void add(String direction, String roomName) {
        Pair p = new Pair(direction, roomName);
        path.add(p);
    }

    public void add(Pair p) {
        path.add(p);
    }

    public boolean contains(String room) {
        for (Pair p: path) {
            if (p.getRoomName().equals(room)) {
                return true;
            }
        }
        return false;
    }

    public Path subPath(String room) {
        if (!contains(room)) {
            return null;
        }
        Path subPath = new Path();
        for (Pair p: path) {
            if (p.getRoomName().equals(room)) {
                subPath.add(p);
                break;
            } else {
                subPath.add(p);
            }
        }
        return subPath;
    }

    @Override
    public String toString() {
        String retval = "Size: " + path.size() + " \n";
        for (Pair p: path) {
            retval += p.getRoomName() + "->";
        }
        retval = retval.substring(0, retval.length()-2);
        return retval;
    }

    public List<Pair> getPath() {
        return path;
    }

    class Pair {
        String incomingDirection;
        String roomName;

        public Pair(String incomingDirection, String roomName) {
            this.incomingDirection = incomingDirection;
            this.roomName = roomName;
        }

        public String getIncomingDirection() {
            return incomingDirection;
        }
        public String getRoomName() {
            return roomName;
        }
    }
}
